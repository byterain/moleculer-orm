"use strict";

const {ServiceBroker} = require("moleculer");
const {ValidationError} = require("moleculer").Errors;
const UsersService = require("../../../services/users.service");

describe("Test 'users' service", () => {
	let broker = new ServiceBroker({logger: false});
	broker.createService(UsersService);
	beforeAll(() => broker.start());
	afterAll(() => broker.stop());

	let id = 0;

	it("Return test for 'users.create'", async () => {
		const res = await broker.call("users.create", {name: "Foo Name", age: 20, items: []});
		expect(typeof res.id).toBe('string');
		expect(res.name).toBe("Foo Name");
		expect(res.age).toBe(20);
		expect(res.items).toStrictEqual([]);
		id = res.id;
	});

	it("Return test for 'users.get'", async () => {
		const res = await broker.call("users.get", {id: id});
		expect(typeof res.id).toBe('string');
		expect(res.name).toBe("Foo Name");
		expect(res.age).toBe(20);
		expect(res.items).toStrictEqual([]);
	});

	it("Return test for 'users.list'", async () => {
		const res = await broker.call("users.list", {});
		expect(res.rows).toBeInstanceOf(Array);
		expect(typeof res.total).toBe("number");
		expect(typeof res.page).toBe("number");
		expect(typeof res.pageSize).toBe("number");
		expect(typeof res.totalPages).toBe("number");
		for (let row of res.rows) {
			expect(typeof row.id).toBe("string");
			expect(typeof row.name).toBe("string");
			expect(typeof row.age).toBe("number");
			expect(row.items).toBeInstanceOf(Array);
		}
	});
	it("Return test for 'users.find'", async () => {
		const res = await broker.call("users.find", {});
		expect(res).toBeInstanceOf(Array);
		for (let item of res) {
			expect(typeof item.id).toBe('string');
			expect(typeof item.name).toBe('string');
			expect(typeof item.age).toBe('number');
			expect(item.items).toBeInstanceOf(Array);
		}
	});
	it("Return test for 'users.count'", async () => {
		const res = await broker.call("users.count", {});
		expect(typeof res).toBe("number");
	});
	it("Return test for 'users.update'", async () => {
		const res = await broker.call("users.update", {id: id, name: "New Foo"});
		expect(res.id).toBe(id);
		expect(res.name).toBe("New Foo");
		expect(res.age).toBe(20);
		expect(res.items).toStrictEqual([]);
	});

	it("Return test for 'users.remove'", async () => {
		const res = await broker.call("users.remove", {id: id});
		expect(res.id).toBe(id);
		expect(res.name).toBe("New Foo");
		expect(res.age).toBe(20);
		expect(res.items).toStrictEqual([]);
	});
});

