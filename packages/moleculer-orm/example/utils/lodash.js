const _ = require('lodash');
_.mixin({
	// Takes an object and another object of strings to strings where the second
	// object describes the key renaming to occur in the first object.
	renameKeys: function (obj, dict, strict = true) {
		let result = strict ? {...obj} : obj;
		for (let key in dict) {
			if (key in result) {
				result[dict[key]] = result[key];
				delete result[key];
			}
		}
		return result;
	},
	renameKey: function (obj, oldKey, newKey, strict = true) {
		let result = strict ? {...obj} : obj;
		if (oldKey in result) {
			result[newKey] = result[oldKey];
			delete result[oldKey];
		}
		return result;
	},
});
