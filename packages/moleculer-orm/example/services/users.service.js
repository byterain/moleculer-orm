"use strict";

const MemoryMixin = require("../mixins/memory.mixin");

module.exports = {
	name: "users",
	mixins: [MemoryMixin],
	model: {
		name: "users",
		fields: {
			name: {type: String, required: true},
			age: {type: Number, required: true},
		},
		relationships: [{service: "items",  primaryKey: "items" ,foreignKey: "_id"}]
	},
	actions: {
		list: {
			include: ["items"]
		},
		get: {
			include: ["items"]
		},
	},
};
