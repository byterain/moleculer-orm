const MoleculerMongoose = require("../../index");
const fs = require("fs")


const MemoryMixin = {
	mixins: [MoleculerMongoose],
	settings: {
		database: {
			path: "./database.json",
		},
	},
	methods: {
		get({where = {},}) {
			return this.model.find(item => Object.keys(where).every(key => where[key] === item[key]));
		},
		async list({where = {}, pageSize = -1, page = 1, order = [], search = {}, fields = []}) {
			let items = this.model.filter(item => Object.keys(where).every(key => where[key] === item[key]));
			let rows = items.splice(page * (pageSize), pageSize);
			return {rows , total: items.length}
		},
		find({where = {}, limit = -1, order = [], search = {}, fields = []}) {
			return this.model.filter(item => Object.keys(where).every(key => where[key] === item[key]));
		},
		create(form) {
			this.model.push(form)
			fs.writeFileSync(this.settings.database.path, JSON.stringify(this.database));
			return form;
		},
		createMany(forms = []) {
			this.model.push(...forms)
			fs.writeFileSync(this.settings.database.path, JSON.stringify(this.database));
			return forms;
		},
		update(doc, updateForm = {}) {
			for (let key in updateForm) {
				doc[key] = updateForm[key];
			}
			fs.writeFileSync(this.settings.database.path, JSON.stringify(this.database));
			return doc;
		},
		remove(doc) {
			let idx = this.model.indexOf(doc);
			this.model.splice(idx, 1);
			fs.writeFileSync(this.settings.database.path, JSON.stringify(this.database));
			return doc;
		},
		removeMany({where = {}}) {
			let itemToRemove = this.model.filter(item => Object.keys(where).every(key => where[key] === item[key]));
			for(let item of itemToRemove){
				let idx = this.model.indexOf(item);
				this.model.splice(idx, 1);
			}
			fs.writeFileSync(this.settings.database.path, JSON.stringify(this.database));
		},
		updateMany(where, form = {}) {
			const itemsToUpdate = this.model.filter(item => Object.keys(where).every(key => where[key] === item[key]));
			for(let item of itemsToUpdate){
				let idx = this.model.indexOf(item);
				this.model.splice(idx, 1);
			}
			return this.model.remove(where);
		},
		count({where = {}, pageSize = -1, page = 1, order = [], search = {}, fields = []}) {
			return this.model.filter(item => Object.keys(where).every(key => where[key] === item[key])).length;
		},
		toJSON(doc) {
			return doc;
		}
	},
	created(){
		const path = this.settings.database.path;
		const model = this.schema.model;
		if(!fs.existsSync(path)) fs.writeFileSync(path, "{}");
		let buffer = fs.readFileSync(path);
		this.database = JSON.parse(buffer.toString());
		if(!this.database[model.name]) {
			this.database[model.name] = [];
			fs.writeFileSync(path, JSON.stringify(this.data));
		}
		this.model = this.database[model.name];
	}
}

module.exports = MemoryMixin;
