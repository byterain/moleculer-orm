"use strict";

const _ = require("lodash");
const {MoleculerClientError} = require("moleculer").Errors;


/**
 * A number, or a string containing a number.
 * @typedef {{where: (String | Object), fields: (String[] | undefined), sort: (String | String[] | undefined) }} GetParams
 */

const ORMMixin = {
    mixins: [],
    settings: {
        /** @type {String} Name of ID field. */
        idField: "_id",

        /** @type {Number} Default page size in `list` action. */
        pageSize: 10,

        /** @type {Number} Maximum page size in `list` action. */
        maxPageSize: 100,

        /** @type {Number} Maximum value of limit in `find` action. Default: `-1` (no limit) */
        maxLimit: -1,

    },
    model: {
        name: "",
        fields: {},

        /** @type {Array?} Schema for population. [Read more](#populating). */
        populate: [{
            service: "items",
            action: 'find',
            type: 'oneToMany',
            primaryId: '_id',
            foreignKey: 'itemId',
            params: {},
        }],
    },
    actions: {
        get: {
            rest: "GET /:id",
            cache: {
                keys: ["id", "fields", "where", "sort"]
            },
            params: {
                id: [
                    {type: "number", integer: true, positive: true, optional: true},
                    {type: "string", optional: true},
                ],
                fields: [
                    {type: "string", optional: true},
                    {type: "array", optional: true, items: "string"}
                ],
                where: [
                    {type: "object", optional: true, default: {}},
                    {type: "string", optional: true, default: "{}"}
                ],
                search: [
                    {type: "object", optional: true, default: {}},
                    {type: "string", optional: true}
                ],
                sort: {type: "string", optional: true},
                populate: {type: "array", optional: true},
            },
            /**
             * Get a document by ID.
             * @param {import("moleculer").Context<GetParams>} ctx
             * @return {Promise<any>}
             */
            async handler(ctx) {
                let params = this.sanitizeParams(ctx.params);
                let query = await this.queryBuilder(ctx, params);
                await this.actionLifeCycle(ctx, "queryHandler", query);
                await this.actionLifeCycle(ctx, "before", query);

                let result = await this.get(query);
                if (!result) throw new MoleculerClientError(this.notFoundError(), 404);
                await this.actionLifeCycle(ctx, "after", result);
                result = this.toJSON(result);
                result = await this.formatResult(ctx, result);
                return result;
            }
        },
        list: {
            rest: "GET /",
            cache: {
                keys: ["fields", "exclude", "page", "pageSize", "sort", "search", "where"]
            },
            params: {
                where: [
                    {type: "object", optional: true},
                    {type: "string", optional: true}
                ],
                search: [
                    {type: "object", optional: true},
                    {type: "string", optional: true}
                ],
                fields: [
                    {type: "string", optional: true},
                    {type: "array", optional: true, items: "string"}
                ],
                page: {type: "number", integer: true, min: 1, optional: true, convert: true},
                pageSize: {type: "number", integer: true, min: 0, optional: true, convert: true},
                sort: {type: "string", optional: true},

            },
            async handler(ctx) {
                let params = this.sanitizeParams(ctx.params);

                let query = await this.queryBuilder(ctx, params);
                let {rows, total} = await this.list(query);
                let result = {
                    rows,
                    total,
                    page: query.page || 1,
                    pageSize: params.pageSize || this.settings.pageSize,
                    totalPages: Math.ceil(total / query.pageSize)
                };
                result.rows = this.toJSON(result.rows);
                result = await this.formatResult(ctx, result);
                return result;
            }
        },
        find: {
            cache: {
                keys: ["fields", "limit", "offset", "sort", "search", "query", "where"]
            },
            params: {
                where: [
                    {type: "object", optional: true},
                    {type: "string", optional: true}
                ],
                limit: {type: "number", integer: true, min: 0, optional: true, convert: true},
                offset: {type: "number", integer: true, min: 0, optional: true, convert: true},
                sort: {type: "string", optional: true},
                search: [
                    {type: "object", optional: true},
                    {type: "string", optional: true}
                ],
                fields: [
                    {type: "string", optional: true},
                    {type: "array", optional: true, items: "string"}
                ]
            },
            async handler(ctx) {
                let params = this.sanitizeParams(ctx.params);
                let query = await this.queryBuilder(ctx, params);
                let result = await this.find(query);
                result = this.toJSON(result);
                result = await this.formatResult(ctx, result);
                return result;
            }
        },
        create: {
            rest: "POST /",
            async handler(ctx) {
                await this.actionLifeCycle(ctx, "before");
                let result = await this.create(ctx.params);
                await this.clearCache();
                await this.actionLifeCycle(ctx, "after");
                result = await this.toJSON(result);
                result = await this.formatResult(ctx, result);
                return result;
            }
        },
        insert: {
            params: {
                $$root: true,
                type: "array",
            },
            async handler(ctx) {
                let entities = ctx.params;
                await this.actionLifeCycle(ctx, "before");
                let result = await this.createMany(entities);
                await this.clearCache();
                await this.actionLifeCycle(ctx, "after");
                result = this.toJSON(result);
                result = await this.formatResult(ctx, result);
                return result;
            }
        },
        update: {
            rest: "PUT /:id",
            params: {
                id: ["number", "string"]
            },
            async handler(ctx) {
                let params = ctx.params;
                let model = await this.get({where: {id: ctx.params.id}});
                if (!model) throw new MoleculerClientError(this.notFoundError(), 404);
                await this.actionLifeCycle(ctx, "before", model);
                await this.update(model, params);
                await this.clearCache();
                await this.actionLifeCycle(ctx, "after", model);
                let result = this.toJSON(model);
                result = await this.formatResult(ctx, result);
                return result;
            }
        },
        remove: {
            rest: "DELETE /:id",
            params: {
                id: {type: "any"}
            },
            async handler(ctx) {
                let model = await this.get({where: {_id: ctx.params.id}});
                if (!model) throw new MoleculerClientError(this.notFoundError(), 404);
                await this.actionLifeCycle(ctx, "before", model);
                await this.remove(model);
                await this.clearCache();
                await this.actionLifeCycle(ctx, "after", model);
                let result = this.toJSON(model);
                result = await this.formatResult(ctx, result, false);
                return result;
            }
        },
        removeMany: {
            params: {
                where: [
                    {type: "string", optional: true},
                    {type: "object", optional: true}
                ]
            },
            async handler(ctx) {
                let params = this.sanitizeParams(ctx.params);
                await this.actionLifeCycle(ctx, "before");
                let result  = await this.removeMany(params);
                await this.clearCache();
                await this.actionLifeCycle(ctx, "after", result);
                result = await this.formatResult(ctx, result);
                return result;
            }
        },
        updateMany: {
            params: {
                where: {type: "object", optional: true},
                value: {type: "object", optional: true},
            },
            async handler(ctx) {
                let params = this.sanitizeParams(ctx.params);
                await this.actionLifeCycle(ctx, "before");
                let result = await this.updateMany(params);
                await this.clearCache();
                await this.actionLifeCycle(ctx, "after", result);
                return params;
            }
        },
        count: {
            cache: {
                keys: ["where", "search"]
            },
            params: {
                search: {type: "string", optional: true},
                where: [
                    {type: "object", optional: true},
                    {type: "string", optional: true}
                ]
            },
            async handler(ctx) {
                let params = this.sanitizeParams(ctx.params);
                if (params && params.limit) params.limit = null;
                if (params && params.offset) params.offset = null;
                await this.actionLifeCycle(ctx, "before");
                let query = await this.queryBuilder(ctx, "count", params);
                let result = await this.count(query);
                await this.actionLifeCycle(ctx, "after", result);
                result = await this.formatResult(ctx, result);
                return result;
            }
        }
    },
    events: {},
    methods: {
        get(query = {}) {
            throw "Implement Method " + arguments.callee.toString()
        },
        list(query = {}) {
            throw "Implement Method " + arguments.callee.toString()
        },
        find(query = {}) {
            throw "Implement Method " + arguments.callee.toString()
        },
        create(form = {}) {
            throw "Implement Method " + arguments.callee.toString()
        },
        createMany(forms = []) {
            throw "Implement Method " + arguments.callee.toString()
        },
        update(query = {}) {
            throw "Implement Method " + arguments.callee.toString()
        },
        /**
         * @param {{where: Object}} query description
         * @param {Object} form description
         */
        updateMany(query = {}, form = {}) {
            throw "Implement Method " + arguments.callee.toString()
        },
        remove(query = {}) {
            throw "Implement Method " + arguments.callee.toString()
        },
        /**
         * @param {{where: Object}} query description
         * @return {Promise<Number>} elements deleted
         */
        removeMany(query = {}) {
            throw "Implement Method " + arguments.callee.toString()
        },
        count(query) {
            throw "Implement Method " + arguments.callee.toString()
        },
        toJSON(doc) {
            throw "Implement Method " + arguments.callee.toString()
        },
        /**
         *
         * @param params {{where: (Object | String | undefined)}}
         * @returns {*}
         */
        sanitizeParams(params) {
            let result = {...params};
            let itemsToParse = ["where"];
            for (let item of itemsToParse) {
                if (typeof params[item] === "string") {
                    try {
                        result[item] = JSON.parse(params[item]);
                    } catch (e) {
                    }
                } else if (params[item] === undefined) {
                    result[item] = {};
                }
            }
            return result;
        },
        async queryBuilder(ctx, params) {
            let query = {...params};
            let action = ctx.action;

            query.where = query.where || {};
            if (query.id) query.where.id = query.id;

            query.fields = action.fields || params.fields;
            query.where = Object.assign({}, params.where, action.where || {});

            query.pageSize = Math.min(params.pageSize || this.settings.pageSize, this.settings.maxPageSize);
            query.page = params.page || 1;
            return query;
        },
        async actionLifeCycle(ctx, event, ...opts) {
            let eventName = _.camelCase(event + "_" + ctx.action.name.split(".")[1]);
            let fn = ctx.action[event] || ctx.action[eventName];
            if (fn) {
                await fn.call(this, ctx, ...opts);
            }
        },
        async queryByRelationship(ctx, where) {
            for (let key in where) {
                if (key.includes(".")) {
                    let [relation, ...rest] = key.split(".");
                    let alias = this.settings.relationships.find(item => item.service === relation);
                    if (alias) {
                        let relationWhere = Object.assign({}, alias.params, {[rest]: where[key]});
                        await ctx.call(alias.service + alias.action, {
                            where: relationWhere,
                            fields: [alias.foreignKey, rest]
                        });
                    }
                }
            }
            return where;
        },
        async formatResult(ctx, result) {
            if (ctx.action.formatResult) {
                let formattedResult = await ctx.action.formatResult.call(this, ctx, result);
                result = formattedResult !== undefined ? formattedResult : result;
            }
            return result;
        },


        notFoundError() {
            let name = this.name;
            if (name.charAt(name.length - 1) === "s") {
                name = name.substring(0, name.length - 1);
            }
            return name.toUpperCase() + "_NOT_FOUND";
        },

        async clearCache(broadcast = true) {
            if (broadcast) this.broker.broadcast(`cache.clean.${this.fullName}`);
            if (this.broker.cacher) return this.broker.cacher.clean(`${this.fullName}.**`);
            return Promise.resolve();
        },

        populateDocs(ctx, docs, populateFields) {
            if (!this.settings.populates || !Array.isArray(populateFields) || populateFields.length == 0)
                return Promise.resolve(docs);

            if (docs == null || !_.isObject(docs) && !Array.isArray(docs))
                return Promise.resolve(docs);

            let promises = [];
            _.forIn(this.settings.populates, (rule, field) => {

                if (populateFields.indexOf(field) === -1)
                    return; // skip

                // if the rule is a function, save as a custom handler
                if (_.isFunction(rule)) {
                    rule = {
                        handler: Promise.method(rule)
                    };
                }

                // If string, convert to object
                if (_.isString(rule)) {
                    rule = {
                        action: rule
                    };
                }

                if (rule.field === undefined) rule.field = field;

                let arr = Array.isArray(docs) ? docs : [docs];

                // Collect IDs from field of docs (flatten, compact & unique list)
                let idList = _.uniq(_.flattenDeep(_.compact(arr.map(doc => _.get(doc, rule.field)))));
                // Replace the received models according to IDs in the original docs
                const resultTransform = (populatedDocs) => {
                    arr.forEach(doc => {
                        let id = _.get(doc, rule.field);
                        if (_.isArray(id)) {
                            let models = _.compact(id.map(id => populatedDocs[id]));
                            _.set(doc, field, models);
                        } else {
                            _.set(doc, field, populatedDocs[id]);
                        }
                    });
                };

                if (rule.handler) {
                    promises.push(rule.handler.call(this, idList, arr, rule, ctx));
                } else if (idList.length > 0) {
                    // Call the target action & collect the promises
                    const params = Object.assign({
                        id: idList,
                        mapping: true,
                        populate: rule.populate
                    }, rule.params || {});

                    promises.push(ctx.call(rule.action, params).then(resultTransform));
                }
            });
            return Promise.all(promises).then(() => docs);
        },
    },

    async started() {
    },
    async created() {
    },
    async merged(schema) {
    }
};

module.exports = ORMMixin;
