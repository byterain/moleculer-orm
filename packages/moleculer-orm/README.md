# Moleculer ORM
[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)


![Moleculer logo](https://github.com/moleculerjs/moleculer/raw/master/docs/assets/logo.png)

Mixin to integrate your favorite ORM with moleculer framework

# Table of contents

| Features                           |
|------------------------------------| 
| 1. [About](#about)                 |


# About <a name="About"></a>
# Moleculer ORM interface
Moleculer-ORM is a backbone to integrate your favorite ORM with moleculer, this interface provides some default CRUD 
actions to your service and integrate with other ORM that implement this lib, so if your project have multiple databases 
types its easy to integrate them together, all you need is instance your ORM lib and implement get, list, find, create, createMany, update,
updateMany, remove, removeMany, count, toJSON methods and take rest with this lib.
