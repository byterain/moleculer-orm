"use strict";

require('./example/utils/lodash');
const mongoose = require('mongoose');
const _ = require('lodash');
const MoleculerOrm = require("moleculer-orm");
const {SchemaType} = require("mongoose");
const {Schema, Types: {ObjectId}} = mongoose;

const connections = [];

function plainObject(object, prefix = ""){
    let result = {};
    for(let key in object){
        const currentPrefix = prefix? prefix + "." + key: key;
        if(typeof object[key] === "object") {
            result = {...result, ...plainObject(object[key], currentPrefix)};
        } else {
            result[currentPrefix] = object[key];
        }
    }
    return result;
}

function deepUpdate(doc, update){
    for(let key in update){
        if(typeof update[key] === "object" && typeof doc[key] === "object"){
            deepUpdate(doc[key], update[key]);
        } else {
            doc[key] = update[key];
        }
    }
}

const MoleculerMongoose = {
    mixins: [MoleculerOrm],
    settings: {
        database: {
            url: "mongodb+srv://cluster0.imiih.mongodb.net/moleculer-orm",
            user: "moleculer-orm",
            pass: "1qZTwR5qudFTXugI",
        },
        idField: '_id',
        versionField: '__v',
    },
    model: {
        name: '',
        fields: {}
    },
    methods: {
        get({where = {}, search}) {
            where = {...where};
            if (where.id) {
                where._id = where.id;
                delete where.id;
            }
            return this.model.findOne(where).exec();
        },
        async list({where = {}, pageSize = -1, page = 1, sort = [], search = {}, fields = []}) {
            where = {...where};
            if (where.id) {
                where._id = where.id;
                delete where.id;
            }
            if (search) {
                if (typeof search === "string") {
                    const paths = this.model.schema.paths;
                    where = this.buildWhereQueryFromSearch(where, paths, search);
                } else if (typeof search === "object") {
                    for (const fieldName in search) {
                        where[fieldName] = {$regex: new RegExp(search[fieldName], 'i')};
                    }
                }
            }
            let builder = this.model.find(where);

            if (pageSize !== -1) {
                builder = builder.limit(pageSize);
                builder = builder.skip(pageSize * (page - 1));
            }

            if(fields){
                if(typeof fields === 'string') fields = [fields];
                builder = builder.select(fields);
            }

            if(sort){
                const sortObj = {};
                if(typeof sort === 'string'){
                    let sortField = sort;
                    const asc = !sort.startsWith("-");
                    if(!asc) sortField = sortField.slice(1);
                    sortObj[sortField] = asc? 1 : -1;
                } else if (Array.isArray(sort)) {
                    for(let sortField of sort){
                        const asc = !sort.startsWith("-");
                        if(!asc) sortField = sortField.slice(1);
                        sortObj[sortField] = asc? 1 : -1;
                    }
                }
                builder = builder.sort(sortObj);
            }

            let [rows, total] = await Promise.all([builder.exec(), this.model.countDocuments(where)])
            return {rows, total}
        },
        find({where = {}, limit = -1, sort = [], search = {}, fields = []}) {
            if (where.id) {
                where._id = where.id;
                delete where.id;
            }

            let builder = this.model.find(where);
            if(sort){
                const sortObj = {};
                if(typeof sort === 'string'){
                    let sortField = sort;
                    const asc = !sort.startsWith("-");
                    if(!asc) sortField = sortField.slice(1);
                    sortObj[sortField] = asc? 1 : -1;
                } else if (Array.isArray(sort)) {
                    for(let sortField of sort){
                        const asc = !sort.startsWith("-");
                        if(!asc) sortField = sortField.slice(1);
                        sortObj[sortField] = asc? 1 : -1;
                    }
                }
                builder = builder.sort(sortObj);
            }
            if (search) {
                if (typeof search === "string") {
                    const paths = this.model.schema.paths;
                    where = this.buildWhereQueryFromSearch(where, paths, search);
                } else if (typeof search === "object") {
                    for (const fieldName in search) {
                        where[fieldName] = {$regex: new RegExp(search[fieldName], 'i')};
                    }
                }
            }

            if (limit !== -1) builder = builder.limit(limit);
            return builder.exec();
        },
        create(form) {
            return this.model.create(form);
        },
        createMany(forms = []) {
            return this.model.insertMany(forms);
        },
        async update(doc, updateForm = {}) {
            await this.model.findByIdAndUpdate(doc._id, {'$set': plainObject(updateForm)}).exec();
            return deepUpdate(doc, updateForm);
        },
        remove(doc) {
            return doc.remove();
        },
        /**
         * @param {{where: Object}} query description
         * @return {Promise<Number>} elements deleted
         */
        async removeMany(query = {}) {
            const res = await this.model.deleteMany(query.where);
            return res.deletedCount || 0;
        },
        /**
         * @param {{where: Object}} query description
         * @param {Object} form description
         * @return {Promise} elements deleted
         */
        updateMany(query, form ) {
            return this.model.findAndModify(query.where, {'$set': plainObject(form)});
        },
        count({where = {}, pageSize = -1, page = 1, order = [], search = {}, fields = []}) {
            return this.model.countDocuments(where)
        },
        toJSON(doc) {
            let isArray = Array.isArray(doc);
            let docs = isArray ? doc.map(item => item.toJSON()) : [doc.toJSON()];
            for (let doc of docs) {
                for (let key in doc) {
                    if (doc[key] instanceof ObjectId) {
                        doc[key] = doc[key].toString();
                    }
                }
                if(doc._id) doc.id = doc._id;
                delete doc._id;
                delete doc.__v;
            }

            return isArray ? docs : docs[0];
        },
        buildWhereQueryFromSearch(where, paths, searchValue, prefix = '') {
            const {versionField, idField} = this.settings;
            const pathNames = Object.keys(paths).filter(item => item !== versionField && item !== idField);
            for (const pathName of pathNames) {
                const path = paths[pathName];
                if (path instanceof Schema.Types.ObjectId) continue;
                if (path instanceof Schema.Types.Array) {
                    if (path.schema) {
                        where = this.buildWhereQueryFromSearch(where, path.schema.paths, searchValue, pathName)
                    }
                } else if (path instanceof Schema.Types.String)  {
                    const fullPath = prefix ? `${prefix}.${pathName}` : pathName;
                    where[fullPath] = {$regex: new RegExp(searchValue, 'i'),};
                }
            }
            return where;
        }
    },
    async created() {
        const {url, user, pass} = this.schema.settings.database;
        const {name, fields} = this.schema.model;
        let conn = connections.find(item => item._connectionString === url);
        if (!conn) {
            this.orm = mongoose.createConnection(url, {user, pass});
            connections.push(this.orm);
        } else {
            this.orm = conn;
        }
        if (!this.model) {
            this.model = this.orm.models[name] ? this.orm.models[name] : this.orm.model(name, new Schema(fields))
        }
    },
};

module.exports = MoleculerMongoose;
