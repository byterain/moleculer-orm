"use strict";

const {ServiceBroker} = require("moleculer");
const {ValidationError} = require("moleculer").Errors;
const UsersService = require("../../../services/users.service");

describe("Test 'users' service", () => {
	let broker = new ServiceBroker({logger: false});
	broker.createService(UsersService);
	beforeAll(() => broker.start());
	afterAll(() => broker.stop());

	let id = 0;

	it("Return test for 'users.create'", async () => {
		const res = await broker.call("users.create", {name: "Foo Name", age: 20, items: []});
		expect(typeof res.id).toBe('string');
		expect(res.name).toBe("Foo Name");
		expect(res.age).toBe(20);
		expect(res.items).toStrictEqual([]);
		id = res.id;
	});

	it("Return test for 'users.get'", async () => {
		const res = await broker.call("users.get", {id: id});
		expect(typeof res.id).toBe('string');
		expect(res.name).toBe("Foo Name");
		expect(res.age).toBe(20);
		expect(res.items).toStrictEqual([]);
	});

	it("Return test for 'users.list'", async () => {
		const res = await broker.call("users.list", {pageSize: 2, page: 2});
		console.log(res);
		expect(res.rows).toBeInstanceOf(Array);
		expect(typeof res.total).toBe("number");
		expect(typeof res.page).toBe("number");
		expect(typeof res.pageSize).toBe("number");
		expect(typeof res.totalPages).toBe("number");
		for (let row of res.rows) {
			expect(typeof row.id).toBe("string");
			expect(typeof row.name).toBe("string");
			expect(typeof row.age).toBe("number");
			expect(row.items).toBeInstanceOf(Array);
		}
	});
	it("search test for 'users.list'", async () => {
		const res = await broker.call("users.list", { search: "Foo", } );
		expect(res.rows).toBeInstanceOf(Array);
		expect(typeof res.total).toBe("number");
		expect(typeof res.page).toBe("number");
		expect(typeof res.pageSize).toBe("number");
		expect(typeof res.totalPages).toBe("number");
		for (let row of res.rows) {
			expect(typeof row.id).toBe("string");
			expect(typeof row.name).toBe("string");
			expect(typeof row.age).toBe("number");
			expect(row.items).toBeInstanceOf(Array);
		}
	});

	it("sort asc test for 'users.list'", async () => {
		const res = await broker.call("users.list", { sort: 'age', } );
		let lastAge = Number.MIN_VALUE;
		for (let row of res.rows) {
			expect(lastAge <= row.age).toBe(true);
			lastAge = row.age;
		}
	});
	it("sort desc test for 'users.list'", async () => {
		const res = await broker.call("users.list", { sort: '-age', } );
		let lastAge = Number.MAX_VALUE;
		for (let row of res.rows) {
			expect(lastAge >= row.age).toBe(true);
			lastAge = row.age;
		}
	});

	it("fields test for 'users.list'", async () => {
		const res = await broker.call("users.list", { fields: ["name", "items"] } );
		expect(res.rows).toBeInstanceOf(Array);
		expect(typeof res.total).toBe("number");
		expect(typeof res.page).toBe("number");
		expect(typeof res.pageSize).toBe("number");
		expect(typeof res.totalPages).toBe("number");
		for (let row of res.rows) {
			expect(typeof row.name).toBe("string");
		}
	});
	it("Return test for 'users.find'", async () => {
		const res = await broker.call("users.find", {});
		expect(res).toBeInstanceOf(Array);
		for (let item of res) {
			expect(typeof item.id).toBe('string');
			expect(typeof item.name).toBe('string');
			expect(typeof item.age).toBe('number');
			expect(item.items).toBeInstanceOf(Array);
		}
	});
	it("Return test for 'users.count'", async () => {
		const res = await broker.call("users.count", {});
		expect(typeof res).toBe("number");
	});
	it("Return test for 'users.update'", async () => {
		const res = await broker.call("users.update", {id: id, name: "New Foo"});
		expect(res.id).toBe(id);
		expect(res.name).toBe("New Foo");
		expect(res.age).toBe(20);
		expect(res.items).toStrictEqual([]);
	});
	it("update inner object for 'users.update'", async () => {
		let res1 = await broker.call("users.update", {id: "6307e02b1187e9fdad1c0598", preferences: {color: 'red'}});
		let res2 = await broker.call("users.update", {id: "6307e02b1187e9fdad1c0598", preferences: {food: 'pizza'}});
		expect(res.id).toBe("6307e02b1187e9fdad1c0598");
		expect(res.name).toBe("New Foo");
		expect(res.age).toBe(20);
		expect(res.items).toStrictEqual([]);
	});


	it("Return test for 'users.remove'", async () => {
		const res = await broker.call("users.remove", {id: id});
		expect(res.id).toBe(id);
		expect(res.name).toBe("New Foo");
		expect(res.age).toBe(20);
		expect(res.items).toStrictEqual([]);
	});
});

