"use strict";

const {ServiceBroker} = require("moleculer");
const {ValidationError} = require("moleculer").Errors;
const UsersService = require("../../services/users.service");

describe("Test 'users' remove actions service", () => {
	let broker = new ServiceBroker({logger: false});
	broker.createService(UsersService);
	beforeAll(() => broker.start());
	afterAll(() => broker.stop());

	it("removeMany Base Test'", async () => {
		const res = await broker.call("users.removeMany", {where: {name: "Foo Name"}});
		expect(typeof res).toBe("number");
	});
});

