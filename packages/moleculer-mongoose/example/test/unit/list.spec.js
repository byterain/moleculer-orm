"use strict";

const {ServiceBroker} = require("moleculer");
const {ValidationError} = require("moleculer").Errors;
const UsersService = require("../../services/users.service");

describe("Test 'users' list actions service", () => {
	let broker = new ServiceBroker({logger: false});
	broker.createService(UsersService);
	beforeAll(() => broker.start());
	afterAll(() => broker.stop());

	it("Return test for 'users.list'", async () => {
		const res = await broker.call("users.list", {});
		expect(res.rows).toBeInstanceOf(Array);
		expect(typeof res.total).toBe("number");
		expect(typeof res.page).toBe("number");
		expect(res.page).toBe(1);
		expect(typeof res.pageSize).toBe("number");
		expect(res.pageSize).toBe(10);
		expect(typeof res.totalPages).toBe("number");
		expect(res.totalPages).toBe(Math.ceil(res.total/res.pageSize));
		for (let row of res.rows) {
			expect(typeof row.id).toBe("string");
			expect(typeof row.name).toBe("string");
			expect(typeof row.age).toBe("number");
			expect(row.items).toBeInstanceOf(Array);
		}
	});

	it("Pagination Test with page less than pageSize", async () => {
		const res = await broker.call("users.list", {page: 1});
		console.log(res);
		expect(res.page).toBe(1);
		expect(res.pageSize).toBe(res.rows.length);
	});
	it("Pagination Test with page equal than pageSize", async () => {
		const res = await broker.call("users.list", {page: 2});
		console.log(res);
		expect(res.page).toBe(2);
		expect(res.pageSize).toBe(10);
		expect(res.rows.length).toBe( res.pageSize - ((res.page * res.pageSize) - res.total) );
	});
	it("Pagination Test with page greater than pageSize", async () => {
		const res = await broker.call("users.list", {page: 999});
		console.log(res);
		expect(res.page).toBe(999);
		expect(res.rows.length).toBe(0);
	});
	it("Pagination Test pageSize test", async () => {
		const res = await broker.call("users.list", {page: 3, pageSize: 2});
		console.log(res);
		expect(res.page).toBe(3);
		expect(res.rows.length).toBe(2);
	});
});

