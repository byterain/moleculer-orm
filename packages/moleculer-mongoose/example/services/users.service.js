"use strict";

const MongooseMixin = require("../../index");
const {Schema} = require("mongoose");

module.exports = {
	name: "users",
	mixins: [MongooseMixin],
	model: {
		name: "users",
		fields: {
			name: {type: String, required: true},
			age: {type: Number, required: true},
			items: {
				type: [{type: Schema.Types.ObjectId, ref: 'items'}],
				default: []
			},
			preferences: {
				color: {type: String, default: ''},
				food: {type: String, default: ''},
			}

		},
		relationships: [{service: "items",  primaryKey: "items" ,foreignKey: "_id"}]
	},
	actions: {
		list: {
			include: ["items"]
		},
		get: {
			include: ["items"]
		},
	},
};
