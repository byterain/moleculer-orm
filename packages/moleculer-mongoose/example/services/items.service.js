"use strict";

const MongooseMixin = require("../mixins/mongoose.mixin");

module.exports = {
	name: "items",
	mixins: [MongooseMixin],
	settings: {},
	model: {
		name: "items",
		fields: {
			name: {type: String, required: true,},
			price: {type: Number, default: 0,},
			quantity: {type: Number, set: Math.round, default: 0},
			description: {type: String, default: ""},
		}
	},
	actions: {
	},
};
